#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/sysinfo.h>
#include <pthread.h>

struct args_simJumps {
	unsigned long long nLeaves;
	unsigned long long maxJump;
	unsigned long long nSimulations;
	unsigned long long* count;
};

unsigned long long min(unsigned long long x, unsigned long long y) { 
	return y ^ ((x ^ y) & -(x < y)); 
}

unsigned long long jumpCount(unsigned long long leavesAhead, unsigned long long maxJump, unsigned int* seedp, unsigned long long curCount) {
	if (leavesAhead == 0)
        return curCount;
    else
		return jumpCount(leavesAhead - (rand_r(seedp) % min(leavesAhead, maxJump) + 1), maxJump, seedp, curCount+1);
}

/*
 * returns overall number of jumps
 */
unsigned long long simJumps(unsigned long long nLeaves, unsigned long long maxJump, unsigned int* seedp, unsigned long long nSimulations) {
	unsigned long long count = 0;
	for (unsigned long long i = 0; i < nSimulations; ++i) {
		count += jumpCount(nLeaves, maxJump, seedp, 0);
	}
	return count;
}

void* simJumps_thr(void* args_ptr) {
	struct args_simJumps* args = (struct args_simJumps*)args_ptr;
	unsigned int seed = (unsigned int)(time(NULL) + pthread_self());
	*(args->count) = simJumps(args->nLeaves, args->maxJump, &seed, args->nSimulations);

	return args->count;
}


unsigned long long simJumpsMultithreaded(unsigned long long nLeaves, unsigned long long maxJump, unsigned long long nSimulations, const size_t nThreads) {
	pthread_t threads[nThreads];
	unsigned long long counts[nThreads];
	struct args_simJumps args[nThreads];

	const unsigned long long simsPerThread = nSimulations / nThreads;
	for (size_t i = 0; i < nThreads; ++i) {
		/* init */
		args[i].nLeaves = nLeaves;
		args[i].maxJump = maxJump;
		args[i].nSimulations = simsPerThread;
		args[i].count = &(counts[i]);

		/* start */
		pthread_create(&(threads[i]), NULL, simJumps_thr, &(args[i]));
	}

	// wait for the threads to finish and collect the results
	unsigned long long count = 0;
	for (size_t i = 0; i < nThreads; ++i) {
		pthread_join(threads[i], NULL);
		count += counts[i];
	}
	return count;
}

int main() {
	const unsigned long long nLeaves = 10;
	const unsigned long long maxJump = 10;
	const unsigned long long nSimulations = 100000000;

	const size_t nProcessors = get_nprocs();
	printf("Running %llu simulations on %zu threads.\n", nSimulations, nProcessors);

	unsigned long long count = simJumpsMultithreaded(nLeaves, maxJump, nSimulations, nProcessors);
	printf("Average number of jumps: %Lf.\n", (long double)count / (long double)nSimulations);

	return EXIT_SUCCESS;
}
