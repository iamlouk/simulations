use sdl2::rect::Rect;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use rayon::prelude::*;
use crate::rand::Rng;

const NEIGHBOURS: [(isize, isize); 8] = [
    (-1, -1), (-1,  0), (-1,  1),
    ( 0, -1),           ( 0,  1),
    ( 1, -1), ( 1,  0), ( 1,  1),
];

const CELLS_X: u32 = 250;
const CELLS_Y: u32 = 250;

#[derive(Clone, Copy, PartialEq)]
enum Cell {
    Alive(u16),
    Dying,
    Dead(u16)
}

struct App {
    canvas: sdl2::render::WindowCanvas,
    cells_prev: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    cells_curr: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,

    iter: usize,
}

impl App {
    fn render(&mut self) { 
        let cells = std::mem::replace(&mut self.cells_curr, None).unwrap();
        self.canvas.set_draw_color(Color::BLACK);
        self.canvas.clear();

        let (window_width, window_height) = self.canvas.window().size();
        let (cell_width, cell_height) = (window_width / CELLS_X, window_height / CELLS_Y);

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                let cell = cells[i][j];
                let color = match cell {
                    Cell::Dead(_) => { continue; },
                    Cell::Dying => Color::RED,
                    Cell::Alive(1) => Color::RGB(0x00, 0x99, 0x33),
                    Cell::Alive(2) => Color::RGB(0x33, 0xcc, 0x33),
                    Cell::Alive(3) => Color::RGB(0x66, 0xff, 0x66),
                    Cell::Alive(4) => Color::RGB(0x99, 0xff, 0x99),
                    Cell::Alive(_) => Color::WHITE
                };
                let x = (i as i32) * (cell_width  as i32);
                let y = (j as i32) * (cell_height as i32);

                self.canvas.set_draw_color(color);
                self.canvas.fill_rect(Rect::new(x, y, cell_width, cell_height)).ok().unwrap();
            }
        }

        self.canvas.present();
        std::mem::replace(&mut self.cells_curr, Some(cells));
    }

    fn update(&mut self) {
        fn wrap_idxs(mut i: isize, mut j: isize, di: isize, dj: isize) -> (usize, usize) {
            i += di;
            j += dj;

            if i < 0 { i = (CELLS_X as isize) + i; } else
            if i >= CELLS_X as isize { i = i - CELLS_X as isize; }
            if j < 0 { j = (CELLS_Y as isize) + j; } else
            if j >= CELLS_Y as isize { j = j - CELLS_Y as isize; }

            (i as usize, j as usize)
        }

        let cells_prev: Box<_> = self.cells_curr.take().unwrap();
        let mut cells_curr: Box<_> = self.cells_prev.take().unwrap();

        cells_curr
            .par_iter_mut()
            .enumerate()
            .for_each(|(i, row)| {
                for j in 0..(CELLS_Y as usize) {
                    let mut alive_neighbours = 0;
                    let mut dead_neighbours = 0;
                    for (di, dj) in NEIGHBOURS.iter() {
                        let (i, j) = wrap_idxs(i as isize, j as isize, *di, *dj);
                        match cells_prev[i][j] {
                            Cell::Alive(n) => alive_neighbours += n,
                            Cell::Dead(n) => dead_neighbours += n,
                            Cell::Dying => {}
                        }
                    }

                    row[j] = match cells_prev[i][j] {
                        Cell::Dying => Cell::Dead(1),
                        Cell::Alive(n) => match (dead_neighbours, alive_neighbours, n) {
                            (_, _, n) if n > 5 => Cell::Dying,
                            (_, a, _) if a < 3 => Cell::Dying,
                            (_, a, n) if a > 2 => Cell::Alive(n + 1),
                            (_, _, n) => Cell::Alive(n)
                        },
                        Cell::Dead(n) => match (dead_neighbours, alive_neighbours, n) {
                            (_, _, n) if n > 4 => Cell::Alive(1),
                            (_, a, n) if a > 2 => Cell::Dead(n + 1),
                            (_, _, n) => Cell::Dead(n),
                        }
                    };
                }
            });

        self.cells_prev = Some(cells_prev);
        self.cells_curr = Some(cells_curr);

        if self.iter % 20 == 0 {
            println!("Update #{:03}",
                     self.iter);
        }
        self.iter += 1;
    }
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    rayon::ThreadPoolBuilder::new().num_threads(4).build_global().unwrap();

    let window = video_subsystem.window("Rust CA", CELLS_X * 3, CELLS_Y * 3)
        .position_centered()
        .build()
        .unwrap();

    let canvas = window.into_canvas().build().unwrap();

    let total_cells = CELLS_X * CELLS_Y;
    let start_cells_alive = total_cells / 7;
    let mut app = App {
        canvas: canvas,
        cells_prev: Some(Box::new(
            [[Cell::Dead(1);
                CELLS_Y as usize]; CELLS_X as usize])),
        cells_curr: Some(Box::new(
            [[Cell::Dead(1);
                CELLS_Y as usize]; CELLS_X as usize])),

        iter: 0,
    };

    let mut rng = rand::thread_rng();
    let cells = &mut app.cells_curr.as_mut().unwrap();
    for i in 0..(CELLS_X as usize) {
        for j in 0..(CELLS_Y as usize) {
            cells[i][j] = if rng.gen_range(0, total_cells) < start_cells_alive {
                Cell::Alive(1)
            } else {
                Cell::Dead(1)
            };
        }
    }

    let mut events = sdl_context.event_pump().unwrap();
    loop {
        for e in events.poll_iter() {
            match e {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    std::process::exit(0);
                },
                _ => {}
            }
        }

        app.render();
        app.update();
        std::thread::sleep(std::time::Duration::from_millis(50));
    }
}

