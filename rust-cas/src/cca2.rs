use sdl2::rect::Rect;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use rayon::prelude::*;
use crate::rand::Rng;

/*
const NEIGHBOURS: [(isize, isize); 36] = [
                        (-3, -1), (-3,  0), (-3,  1),
              (-2, -2), (-2, -1), (-2,  0), (-2,  1), (-2,  2),
    (-1, -3), (-1, -2), (-1, -1), (-1,  0), (-1,  1), (-1,  2), (-1,  3),
    ( 0, -3), ( 0, -2), ( 0, -1),           ( 0,  1), ( 0,  2), ( 0,  3),
    ( 1, -3), ( 1, -2), ( 1, -1), ( 1,  0), ( 1,  1), ( 1,  2), ( 1,  3),
              ( 2, -2), ( 2, -1), ( 2,  0), ( 2,  1), ( 2,  2),
                        ( 3, -1), ( 3,  0), ( 3,  1)
];
*/

static mut NEIGHBOURS: [(isize, isize); 432] = [(0, 0); 432];

const CELLS_X: u32 = 800;
const CELLS_Y: u32 = 800;

type Cell = bool;

struct App {
    canvas: sdl2::render::WindowCanvas,
    cells_prev: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    cells_curr: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,

    iter: usize,
}

impl App {
    fn render(&mut self) { 
        let cells = std::mem::replace(&mut self.cells_curr, None).unwrap();
        self.canvas.set_draw_color(Color::BLACK);
        self.canvas.clear();

        let (window_width, window_height) = self.canvas.window().size();
        let (cell_width, cell_height) = (window_width / CELLS_X, window_height / CELLS_Y);

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                let cell = cells[i][j];
                // let color = Color::RGB((cell * 255.0) as u8, (cell * 255.0) as u8, (cell * 255.0) as u8);

                let color = match cell {
                    true => Color::WHITE,
                    false => Color::BLACK
                };

                let x = (i as i32) * (cell_width  as i32);
                let y = (j as i32) * (cell_height as i32);

                self.canvas.set_draw_color(color);
                self.canvas.fill_rect(Rect::new(x, y, cell_width, cell_height)).ok().unwrap();
            }
        }

        self.canvas.present();
        std::mem::replace(&mut self.cells_curr, Some(cells));
    }

    fn update(&mut self) {
        fn wrap_idxs(mut i: isize, mut j: isize, di: isize, dj: isize) -> (usize, usize) {
            i += di;
            j += dj;

            if i < 0 { i = (CELLS_X as isize) + i; } else
            if i >= CELLS_X as isize { i = i - CELLS_X as isize; }
            if j < 0 { j = (CELLS_Y as isize) + j; } else
            if j >= CELLS_Y as isize { j = j - CELLS_Y as isize; }

            (i as usize, j as usize)
        }

        let cells_prev: Box<_> = self.cells_curr.take().unwrap();
        let mut cells_curr: Box<_> = self.cells_prev.take().unwrap();

        cells_curr
            .par_iter_mut()
            .enumerate()
            .for_each(|(i, row)| {
                for j in 0..(CELLS_Y as usize) {
                    let cell = cells_prev[i][j];
                    let sum = unsafe { NEIGHBOURS.iter() }
                        .map(|(di, dj)| wrap_idxs(i as isize, j as isize, *di, *dj))
                        .fold(0, |sum, (x, y)| if cells_prev[x][y] { 1 } else { 0 } + sum);
                    let avg = sum as f32 / unsafe { NEIGHBOURS.len() } as f32;

                    row[j] = match (cell, avg) {
                        (false, a) if a > 0.3 && a < 0.4 => true,
                        (true, a)  if a > 0.5 => false,
                        (true, a)  if a < 0.2 => false,
                        (true, _) => true,
                        (false, _) => false
                    };

                    // row[j] = 1. / (1. + (-x).exp());
                    // row[j] = if x < 0.0 { 0.0 } else if x > 1.0 { 1.0 } else { x };
                }
            });

        self.cells_prev = Some(cells_prev);
        self.cells_curr = Some(cells_curr);

        self.iter += 1;
    }
}

fn init_neighbours() {
    let mut i = 0;
    for x in (-10)..11 {
        for y in (-10)..11 {
            if (x <= 1 && x >= -1) && (y <= 1 && y >= -1) {
                continue;
            }

            unsafe {
                NEIGHBOURS[i] = (x, y);
            }
            i += 1;
        }
    }
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    rayon::ThreadPoolBuilder::new().num_threads(4).build_global().unwrap();

    let window = video_subsystem.window("Rust CA", CELLS_X * 1, CELLS_Y * 1)
        .position_centered()
        .build()
        .unwrap();

    let canvas = window.into_canvas().build().unwrap();

    let mut app = App {
        canvas: canvas,
        cells_prev: Some(Box::new(
            [[false; CELLS_Y as usize]; CELLS_X as usize])),
        cells_curr: Some(Box::new(
            [[false; CELLS_Y as usize]; CELLS_X as usize])),

        iter: 0,
    };

    init_neighbours();

    let mut rng = rand::thread_rng();
    let cells = &mut app.cells_curr.as_mut().unwrap();
    for i in 0..(CELLS_X as usize) {
        for j in 0..(CELLS_Y as usize) {
            cells[i][j] = if rng.gen_range(0.0, 1.0) < 0.5 { false } else { true };
        }
    }

    let mut events = sdl_context.event_pump().unwrap();
    loop {
        for e in events.poll_iter() {
            match e {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    std::process::exit(0);
                },
                _ => {}
            }
        }

        app.render();
        app.update();
    }
}

