
use sdl2::rect::Rect;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use crate::rand::Rng;

const NEIGHBOURS: [(isize, isize); 8] = [
    (-1, -1), (-1,  0), (-1,  1),
    ( 0, -1),           ( 0,  1),
    ( 1, -1), ( 1,  0), ( 1,  1),
];

const CELLS_X: u32 = 200;
const CELLS_Y: u32 = 200;

#[derive(Clone, Copy, PartialEq)]
struct Cell {
    ants: u16,
    ants_next: u16,
    mark: f32
}

struct App {
    canvas: sdl2::render::WindowCanvas,
    cells_prev: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    cells_curr: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,

    iter: usize,
    total_ants: u32,
    total_cells: u32
}

impl App {
    fn render(&mut self) { 
        let cells = std::mem::replace(&mut self.cells_curr, None).unwrap();
        self.canvas.set_draw_color(Color::BLACK);
        self.canvas.clear();

        let (window_width, window_height) = self.canvas.window().size();
        let (cell_width, cell_height) = (window_width / CELLS_X, window_height / CELLS_Y);
        

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                let cell = cells[i][j];
                let color = match cell.ants {
                    0 => { continue; },
                    3 => Color::RGB(0x99, 0xff, 0x99),
                    2 => Color::RGB(0x66, 0xff, 0x66),
                    1 => Color::RGB(0x33, 0xcc, 0x33),
                    _ => Color::RED // Color::RGB(0x00, 0x99, 0x33)
                };
                let x = (i as i32) * (cell_width  as i32);
                let y = (j as i32) * (cell_height as i32);

                self.canvas.set_draw_color(color);
                self.canvas.fill_rect(Rect::new(x, y, cell_width, cell_height)).ok().unwrap();
            }
        }

        self.canvas.present();
        std::mem::replace(&mut self.cells_curr, Some(cells));
    }

    fn update(&mut self) {
        let mut rng = rand::thread_rng();
        let mut ants = 0;
        let mut avg_mark = 0.0;
        let mut all_cells_max_mark = f32::MIN;

        fn wrap_idxs(mut i: isize, mut j: isize, di: isize, dj: isize) -> (usize, usize) {
            i += di;
            j += dj;

            if i == -1 { i = (CELLS_X as isize) - 1; } else
            if i == CELLS_X as isize { i = 0; }
            if j == -1 { j = (CELLS_Y as isize) - 1; } else
            if j == CELLS_Y as isize { j = 0; }

            (i as usize, j as usize)
        }

        let cells_prev: Box<_> = self.cells_curr.take().unwrap();
        let mut cells_curr: Box<_> = self.cells_prev.take().unwrap();

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                let mut cell = cells_prev[i][j];
                ants += cell.ants as u32;

                let mut neighbours_marks = 0.;
                let mut neighbours_ants = 0;
                let mut max_mark = f32::MIN;
                let mut max_mark_ants = 0;
                let (mut max_di, mut max_dj) = (0, 0);
                for (di, dj) in NEIGHBOURS.iter() {
                    let (i, j) = wrap_idxs(i as isize, j as isize, *di, *dj);
                    let neighbour = cells_prev[i][j];
                    neighbours_marks += neighbour.mark;
                    neighbours_ants += neighbour.ants;
                    if neighbour.mark > max_mark {
                        max_mark = neighbour.mark;
                        max_di = *di;
                        max_dj = *dj;
                        max_mark_ants = neighbour.ants;
                    }
                }

                neighbours_marks /= 8.;
                cell.mark = (cell.mark * 0.5 + neighbours_marks * 1.5) / 2.;
                cell.mark += cell.ants as f32;
                cell.mark += neighbours_ants as f32;

                let num_ants = cell.ants;
                for _ in 0..num_ants {
                    let r: f32 = rng.gen_range(0., 1.);
                    if max_mark_ants < 2 && r > 0.5 {
                        cell.ants -= 1;
                        cell.mark -= 5.;
                        let (i, j) = wrap_idxs(i as isize, j as isize, max_di, max_dj);
                        cells_curr[i][j].ants_next += 1;
                        continue;
                    }

                    if r < 0.5 {
                        cell.ants -= 1;
                        cell.mark -= 5.;
                        let (di, dj) = NEIGHBOURS[rng.gen_range(0, 8)];
                        let (i, j) = wrap_idxs(i as isize, j as isize, di, dj);
                        cells_curr[i][j].ants_next += 1;
                        continue;
                    }
                }

                avg_mark += cell.mark;
                all_cells_max_mark = f32::max(all_cells_max_mark, cell.mark);
                cells_curr[i][j].ants = cell.ants;
                cells_curr[i][j].mark = cell.mark;
            }
        }

        avg_mark /= self.total_cells as f32;

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                cells_curr[i][j].ants += cells_curr[i][j].ants_next;
                cells_curr[i][j].ants_next = 0;
                cells_curr[i][j].mark -= avg_mark;
            }
        }
 
        self.total_ants = ants;
        self.cells_prev = Some(cells_prev);
        self.cells_curr = Some(cells_curr);

        if self.iter % 20 == 0 {
            println!("Update #{:03}: (avg_mark={:.03}, max_mark={:.03})",
                     self.iter, avg_mark, all_cells_max_mark);
        }
        self.iter += 1;
    }
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("Rust CA", 1000, 1000)
        .position_centered()
        .build()
        .unwrap();

    let canvas = window.into_canvas().build().unwrap();

    let total_cells = CELLS_X * CELLS_Y;
    let start_ants = total_cells / 100;
    let mut app = App {
        canvas: canvas,
        cells_prev: Some(Box::new(
            [[Cell { ants: 0, mark: 0., ants_next: 0 };
                CELLS_Y as usize]; CELLS_X as usize])),
        cells_curr: Some(Box::new(
            [[Cell { ants: 0, mark: 0., ants_next: 0 };
                CELLS_Y as usize]; CELLS_X as usize])),

        iter: 0,
        total_ants: start_ants,
        total_cells: total_cells
    };

    let mut rng = rand::thread_rng();
    let cells = &mut app.cells_curr.as_mut().unwrap();
    for i in 0..(CELLS_X as usize) {
        for j in 0..(CELLS_Y as usize) {
            cells[i][j] = Cell {
                ants: if rng.gen_range(0, total_cells) < start_ants { 1 } else { 0 },
                mark: rng.gen_range(-10., 10.),
                ants_next: 0
            };
        }
    }

    let mut events = sdl_context.event_pump().unwrap();
    loop {
        for e in events.poll_iter() {
            match e {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    std::process::exit(0);
                },
                _ => {}
            }
        }

        app.render();
        app.update();
        std::thread::sleep(std::time::Duration::from_millis(25));
    }
}

