#![feature(assoc_int_consts)]

extern crate sdl2;
extern crate rand;
extern crate rayon;

mod random_ants;
mod cells;
mod spirals;
mod circuits;
mod cca;
mod cca2;

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() != 2 {
        eprintln!("Use `--help`!");
        std::process::exit(1);
    }

    match args[1].as_ref() {
        "--random-ants" => random_ants::main(),
        "--spirals" => spirals::main(),
        "--circuits" => circuits::main(),
        "--cells" => cells::main(),
        "--cca" => cca::main(),
        "--cca2" => cca2::main(),
        "--help" => {
            println!("possible arguments: `--random-ants`, `--spirals`, `--circuits`, `--cells`, `--cca`");
        },
        _ => {
            eprintln!("Use `--help`!");
            std::process::exit(1);
        }
    }
}

