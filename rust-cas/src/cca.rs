use sdl2::rect::Rect;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use rayon::prelude::*;
use crate::rand::Rng;

const CELLS_X: u32 = 500;
const CELLS_Y: u32 = 500;
const OUTER_CIRCLE_RADIUS: f32 = 15.;
const INNER_CIRCLE_RADIUS: f32 = 5.;
const INNER_CIRCLE_FACTOR: f32 = -7.;

type Cell = f32;

struct App {
    canvas: sdl2::render::WindowCanvas,
    cells_prev: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    cells_curr: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    iter: usize,

    neighbours: Vec<(isize, isize)>,
    weights: Vec<f32>,
    sum_norm_factor: f32,
}

impl App {
    fn render(&mut self) { 
        let cells = std::mem::replace(&mut self.cells_curr, None).unwrap();
        self.canvas.set_draw_color(Color::BLACK);
        self.canvas.clear();

        let (window_width, window_height) = self.canvas.window().size();
        let (cell_width, cell_height) = (window_width / CELLS_X, window_height / CELLS_Y);

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                let cell = cells[i][j];
                let abs = cell.abs();


                let color = match cell > 0. {
                    false => Color::RGB((abs * 255.0) as u8, 0, 0),
                    true => Color::RGB(0, (abs * 255.0) as u8, 0),
                };

                // let color = Color::RGB((abs * 255.0) as u8, (abs * 255.0) as u8, (abs * 255.0) as u8);
 
                let x = (i as i32) * (cell_width  as i32);
                let y = (j as i32) * (cell_height as i32);

                self.canvas.set_draw_color(color);
                self.canvas.fill_rect(Rect::new(x, y, cell_width, cell_height)).ok().unwrap();
            }
        }

        self.canvas.present();
        std::mem::replace(&mut self.cells_curr, Some(cells));
    }

    fn update(&mut self) {
        fn wrap_idxs(mut i: isize, mut j: isize, di: isize, dj: isize) -> (usize, usize) {
            i += di;
            j += dj;

            if i < 0 { i = (CELLS_X as isize) + i; } else
            if i >= CELLS_X as isize { i = i - CELLS_X as isize; }
            if j < 0 { j = (CELLS_Y as isize) + j; } else
            if j >= CELLS_Y as isize { j = j - CELLS_Y as isize; }

            (i as usize, j as usize)
        }

        let cells_prev: Box<_> = self.cells_curr.take().unwrap();
        let mut cells_curr: Box<_> = self.cells_prev.take().unwrap();

        let neighbours = &self.neighbours;
        let weights = &self.weights;
        let sum_norm_factor = self.sum_norm_factor;

        cells_curr
            .par_iter_mut()
            .enumerate()
            .for_each(|(i, row)| {
                for j in 0..(CELLS_Y as usize) {
                    let cell = cells_prev[i][j];

                    let wn = neighbours.iter()
                        .map(|(di, dj)| wrap_idxs(i as isize, j as isize, *di, *dj))
                        .zip(weights.iter())
                        .fold(0., |sum, ((x, y), w)| cells_prev[x][y] * w + sum)
                            * sum_norm_factor;

                    let x = match (cell, wn) {
                        (c, wn) if wn.abs() > 0.66 => c * 0.9,
                        (c, wn) if c > 0. && wn > 0. => c * 1.1,
                        (c, wn) if c < 0. && wn < 0. => c * 1.1,
                        (c, wn) if c.abs() < 0.05 => 0.1 * wn.signum(),
                        (c, _) => c * 0.9 
                    };

                    row[j] = if x > 1. { 1. } else if x < -1. { -1. } else { x };
                }
            });

        self.cells_prev = Some(cells_prev);
        self.cells_curr = Some(cells_curr);

        print!("Iter #{}\r", self.iter);
        use std::io::Write;
        std::io::stdout().flush().unwrap();
        self.iter += 1;
    }

    pub fn init_neighbours(&mut self) {
        let n = OUTER_CIRCLE_RADIUS as isize;
        let mut sum_norm_factor = 0.;
        for x in (-n)..(n+1) {
            for y in (-n)..(n+1) {
                let mut d = ((x * x + y * y) as f32).sqrt();
                if d > OUTER_CIRCLE_RADIUS || (x == 0 && y == 0) {
                    continue;
                }

                if d < INNER_CIRCLE_RADIUS {
                    d *= INNER_CIRCLE_FACTOR;
                }

                sum_norm_factor += d;
                self.weights.push(d);
                self.neighbours.push((x, y));
            }
        }

        self.sum_norm_factor = 1. / sum_norm_factor;
    }
}



pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    // rayon::ThreadPoolBuilder::new().num_threads(4).build_global().unwrap();

    let window = video_subsystem.window("Rust CA", CELLS_X * 1, CELLS_Y * 1)
        .position_centered()
        .build()
        .unwrap();

    let canvas = window.into_canvas().build().unwrap();

    let mut app = App {
        canvas: canvas,
        cells_prev: Some(Box::new(
            [[0.; CELLS_Y as usize]; CELLS_X as usize])),
        cells_curr: Some(Box::new(
            [[0.; CELLS_Y as usize]; CELLS_X as usize])),

        iter: 0,
        neighbours: Vec::new(),
        weights: Vec::new(),
        sum_norm_factor: 0.
    };

    app.init_neighbours();

    let mut rng = rand::thread_rng();
    let cells = &mut app.cells_curr.as_mut().unwrap();
    for i in 0..(CELLS_X as usize) {
        for j in 0..(CELLS_Y as usize) {
            cells[i][j] = rng.gen_range(-1.0, 1.0);
        }
    }

    let mut events = sdl_context.event_pump().unwrap();
    loop {
        for e in events.poll_iter() {
            match e {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    println!();
                    std::process::exit(0);
                },
                _ => {}
            }
        }

        app.render();
        app.update();
    }
}

