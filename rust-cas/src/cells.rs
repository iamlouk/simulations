
use sdl2::rect::Rect;
use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use crate::rand::Rng;
// use rayon::prelude::*;

const NEIGHBOURS: [(isize, isize); 8] = [
    (-1, -1), (-1,  0), (-1,  1),
    ( 0, -1),           ( 0,  1),
    ( 1, -1), ( 1,  0), ( 1,  1),
];

const CELLS_X: u32 = 250;
const CELLS_Y: u32 = 250;
const TEAMS: usize = 3;
const MAX_ANTS: u16 = 15;

#[derive(Clone, Copy, PartialEq)]
struct Cell {
    ants:  [u16; TEAMS],
    marks: [f32; TEAMS]
}

struct App {
    canvas: sdl2::render::WindowCanvas,
    cells_prev: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    cells_curr: Option<Box<[[Cell; CELLS_Y as usize]; CELLS_X as usize]>>,
    avg_marks: [f32; TEAMS],
}

impl App {
    fn render(&mut self) { 
        let cells = std::mem::replace(&mut self.cells_curr, None).unwrap();
        self.canvas.set_draw_color(Color::BLACK);
        self.canvas.clear();

        let (window_width, window_height) = self.canvas.window().size();
        let (cell_width, cell_height) = (window_width / CELLS_X, window_height / CELLS_Y);

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                let cell = cells[i][j];

                let r = 0xf * cell.ants[0] as u8;
                let g = 0xf * cell.ants[1] as u8;
                let b = 0xf * cell.ants[2] as u8;

                let x = (i as i32) * (cell_width  as i32);
                let y = (j as i32) * (cell_height as i32);
                self.canvas.set_draw_color(Color::RGB(r, g, b));
                self.canvas.fill_rect(
                    Rect::new(x, y, cell_width, cell_height)).ok().unwrap();
            }
        }

        self.canvas.present();
        std::mem::replace(&mut self.cells_curr, Some(cells));
    }

    fn update(&mut self) {
        // let mut rng = rand::thread_rng();
        let mut avg_marks: [f32; TEAMS] = [0.0; TEAMS];

        fn wrap_idxs(mut i: isize, mut j: isize, di: isize, dj: isize) -> (usize, usize) {
            i += di;
            j += dj;

            if i == -1 { i = (CELLS_X as isize) - 1; } else
            if i == CELLS_X as isize { i = 0; }
            if j == -1 { j = (CELLS_Y as isize) - 1; } else
            if j == CELLS_Y as isize { j = 0; }

            (i as usize, j as usize)
        }

        let cells_prev: Box<_> = self.cells_curr.take().unwrap();
        let mut cells_curr: Box<_> = self.cells_prev.take().unwrap();

        for i in 0..(CELLS_X as usize) {
        // cells_curr
        //    .par_iter_mut()
        //    .enumerate()
        //    .for_each(|(i, row)| {
                for j in 0..(CELLS_Y as usize) {
                    let mut cell = cells_prev[i][j];
                    let mut marks: [f32; TEAMS] = [0.0; TEAMS];
                    let mut ants: [u16; TEAMS] = [0; TEAMS];

                    for (di, dj) in NEIGHBOURS.iter() {
                        let (i, j) = wrap_idxs(i as isize, j as isize, *di, *dj);
                        for t in 0..TEAMS {
                            marks[t] += cells_prev[i][j].marks[t];
                            ants[t] += cells_prev[i][j].ants[t];
                        }
                    }

                    for t in 0..TEAMS {
                        cell.marks[t] += cell.ants[t] as f32;
                        cell.marks[t] = (marks[t] + cell.marks[t]) / 9.0;
                        avg_marks[t] += cell.marks[t];
                    }

                    for t in 0..TEAMS {
                        let mut other_marks = 0.0;
                        for ot in 0..TEAMS {
                            if t != ot {
                                other_marks += cell.marks[ot];
                            }
                        }

                        if (cell.marks[t] < 0.0 || other_marks / (TEAMS as f32 - 1.) > cell.marks[t]) && cell.ants[t] > 0 {
                            cell.ants[t] -= 1;
                        }

                        if cell.marks[t] > 0.0 && other_marks < cell.marks[t] && cell.ants[t] < MAX_ANTS {
                            cell.ants[t] += 1;
                        }
                    }

        //            row[j] = cell;
                    cells_curr[i][j] = cell;
                }
        //    });
        }

        for t in 0..TEAMS {
            avg_marks[t] /= (CELLS_X * CELLS_Y) as f32;
        }

        for i in 0..(CELLS_X as usize) {
            for j in 0..(CELLS_Y as usize) {
                for t in 0..TEAMS {
                    cells_curr[i][j].marks[t] -= avg_marks[t];
                }
            }
        }

        self.avg_marks = avg_marks;
        self.cells_prev = Some(cells_prev);
        self.cells_curr = Some(cells_curr);
    }
}

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    rayon::ThreadPoolBuilder::new().num_threads(4).build_global().unwrap();

    let window = video_subsystem.window("Rust CA", 750, 750)
        .position_centered()
        .build()
        .unwrap();

    let canvas = window.into_canvas().build().unwrap();

    let mut app = App {
        canvas: canvas,
        cells_prev: Some(Box::new(
            [[Cell { ants: [0; TEAMS], marks: [0.0; TEAMS] };
                CELLS_Y as usize]; CELLS_X as usize])),
        cells_curr: Some(Box::new(
            [[Cell { ants: [0; TEAMS], marks: [0.0; TEAMS] };
                CELLS_Y as usize]; CELLS_X as usize])),
        avg_marks: [0.0; TEAMS]
    };

    let mut rng = rand::thread_rng();
    let cells = &mut app.cells_curr.as_mut().unwrap();
    for i in 0..(CELLS_X as usize) {
        for j in 0..(CELLS_Y as usize) {
            for idx in 0..TEAMS {
                cells[i][j].ants[idx] = rng.gen_range(0, MAX_ANTS);
            }
        }
    }

    let mut events = sdl_context.event_pump().unwrap();
    loop {
        for e in events.poll_iter() {
            match e {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    std::process::exit(0);
                },
                _ => {}
            }
        }

        app.render();
        app.update();
        std::thread::sleep(std::time::Duration::from_millis(10));
    }
}

