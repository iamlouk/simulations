extern crate console_error_panic_hook;

use std::{u32, f64};
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsCast, JsValue};

const NEIGHBOURS: [(isize, isize); 8] = [
    (-1, -1), (-1,  0), (-1,  1),
    ( 0, -1),           ( 0,  1),
    ( 1, -1), ( 1,  0), ( 1,  1),
];

macro_rules! log {
    ( $( $t:tt )* ) => {
        web_sys::console::log_1(&format!( $( $t )* ).into());
    }
}

type State = u32;

#[derive(Copy, Clone, Debug)]
enum Transition {
    Always(State),
    Randomly(State, f32),
    IfLt(State, State, u16),
    IfEq(State, State, u16),
    IfGt(State, State, u16)
}

#[wasm_bindgen]
pub struct CAEngine {
    width: f64,
    height: f64,
    ctx: web_sys::CanvasRenderingContext2d,

    cell_size: f64,
    cell_rows: usize,
    cell_cols: usize,

    colors: Vec<JsValue>,
    rules: Vec<Vec<Transition>>,

    grid_1: Option<Vec<State>>,
    grid_2: Option<Vec<State>>
}

#[wasm_bindgen]
impl CAEngine {
    pub fn new() -> Self {
        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .map_err(|_| ())
            .unwrap();

        let (width, height) = (canvas.width(), canvas.height());

        let ctx = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();

        ctx.set_font("20px monospace");

        let cell_size = 5;
        let cell_rows = (width / cell_size) as usize;
        let cell_cols = (height / cell_size) as usize;

        CAEngine {
            width: width as f64,
            height: height as f64,
            ctx,

            cell_size: cell_size as f64,
            cell_rows,
            cell_cols,

            colors: vec![],
            rules: vec![],

            grid_1: Some(vec![0; cell_rows * cell_cols]),
            grid_2: Some(vec![0; cell_rows * cell_cols])
        }
    }

    pub fn set_states(&mut self, colors: Vec<JsValue>) {
        self.rules.clear();
        for _ in &colors {
            self.rules.push(vec![]);
        }

        self.colors = colors;
    }

    pub fn random_gird_init(&mut self, state_a: State, state_b: State, state_b_prop: f64) {
        let mut grid = self.grid_1.take().unwrap();

        for i in 0..self.cell_rows {
            for j in 0..self.cell_cols {
                let r = js_sys::Math::random();
                if r < state_b_prop {
                    grid[i * self.cell_cols + j] = state_b;
                } else {
                    grid[i * self.cell_cols + j] = state_a;
                }
            }
        }

        self.grid_1 = Some(grid);
    }

    pub fn add_rule_always(&mut self, from_state: State, to_state: State) {
        self.rules[from_state as usize].push(
            Transition::Always(to_state));
    }

    pub fn add_rule_randomly(&mut self, from_state: State, to_state: State, prop: f32) {
        assert!(0.0 <= prop && prop <= 1.0);
        self.rules[from_state as usize].push(
            Transition::Randomly(to_state, prop));
    }

    pub fn add_rule_lt(&mut self, from_state: State, to_state: State,
            counted_state: State, count: u16) {
        assert!(count <= 8);
        self.rules[from_state as usize].push(
            Transition::IfLt(to_state, counted_state, count));
    }

    pub fn add_rule_eq(&mut self, from_state: State, to_state: State,
            counted_state: State, count: u16) {
        assert!(count <= 8);
        self.rules[from_state as usize].push(
            Transition::IfEq(to_state, counted_state, count));
    }

    pub fn add_rule_gt(&mut self, from_state: State, to_state: State,
            counted_state: State, count: u16) {
        assert!(count <= 8);
        self.rules[from_state as usize].push(
            Transition::IfGt(to_state, counted_state, count));
    }

    fn wrap_idxs(&self, mut i: isize, mut j: isize, di: isize, dj: isize) -> usize {
        i += di;
        j += dj;

        if i < 0 { i = (self.cell_rows as isize) + i; } else
        if i >= self.cell_rows as isize { i = i - self.cell_rows as isize; }
        if j < 0 { j = (self.cell_cols as isize) + j; } else
        if j >= self.cell_cols as isize { j = j - self.cell_cols as isize; }

        (i * self.cell_cols as isize + j) as usize
    }

    pub fn update(&mut self) {
        let grid_1 = self.grid_1.take().unwrap();
        let mut grid_2 = self.grid_2.take().unwrap();

        let mut states = vec![0; self.colors.len()];

        for i in 0..self.cell_rows {
            for j in 0..self.cell_cols {
                for idx in 0..self.colors.len() {
                    states[idx] = 0;
                }

                for (di, dj) in NEIGHBOURS.iter() {
                    let pos = self.wrap_idxs(i as isize, j as isize, *di, *dj);
                    states[grid_1[pos] as usize] += 1;
                }

                let idx = i * self.cell_cols + j;
                grid_2[idx] = grid_1[idx];
                for rule in &self.rules[grid_1[idx] as usize] {
                    grid_2[idx] = match *rule {
                        Transition::Always(to_state) => to_state,
                        Transition::Randomly(to_state, prop)
                            if (js_sys::Math::random() as f32) < prop => to_state,
                        Transition::IfLt(to_state, count_state, count)
                            if states[count_state as usize] < count => to_state,
                        Transition::IfEq(to_state, count_state, count)
                            if states[count_state as usize] == count => to_state,
                        Transition::IfGt(to_state, count_state, count)
                            if states[count_state as usize] > count => to_state,
                        _ => continue
                    };
                    break;
                }
            }
        }

        self.grid_1 = Some(grid_2);
        self.grid_2 = Some(grid_1);
    }

    pub fn render(&mut self) {
        self.ctx.clear_rect(0.0, 0.0, self.width, self.height);

        let grid = self.grid_1.take().unwrap();

        for i in 0..self.cell_rows {
            for j in 0..self.cell_cols {
                let x = i as f64 * self.cell_size;
                let y = j as f64 * self.cell_size;

                self.ctx.set_fill_style(
                    &self.colors[grid[i * self.cell_cols + j] as usize]
                );
                self.ctx.fill_rect(x, y, self.cell_size, self.cell_size);
            }
        }

        self.grid_1 = Some(grid);
    }

}

#[wasm_bindgen(start)]
pub fn start() {
    // log!("Hello Browser-Console!");
    console_error_panic_hook::set_once();
}
